<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tugas Hana</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
  </head>
  <body>

  <div class="col-8 offset-2 card shadow p-3">
    <form action="welcome">
        <h3>Form Tambah Data</h3>
        <div class="mb-3">
            <label for="">Nama Depan</label>
            <input type="text" class="form-control" placeholder="Masukkan Nama Depan Anda">
        </div>
        <div class="mb-3">
            <label for="">Nama Belakang</label>
            <input type="text" class="form-control" placeholder="Masukkan Nama Belakang Anda">
        </div>
        <div class="mb-3">
            <label for="">No Telepon</label>
            <input type="text" class="form-control" placeholder="Masukkan No Telepon Anda">
        </div>
        <div class="mb-3">
            <label for="">ALamat</label>
            <input type="text" class="form-control" placeholder="Masukkan ALamat Anda">
        </div>

        <div class="mb-3">
            <button class="btn btn-primary">Kirim</button>
        </div>
    </form>
  </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
  </body>
</html>